#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 100
struct BOOK
{
	char title[N];
	char author[N];
	int year;
};
void get_book(struct BOOK *line, int tmp)
{
	FILE *file;
	int i;
	char str[N];
	file = fopen("book.txt", "r");
	for(i = 0; i < tmp; i++)
        {
        fgets(line[i].title, N, file);
		line[i].title[strlen(line[i].title)-1] = '\0';
        fgets(line[i].author, N, file);
		line[i].author[strlen(line[i].author)-1] = '\0';
        fgets(str, N, file);
        line[i].year = atoi(str); //atoi ����������� ������, ���������� ���������� str, � �������� ���� int.
		fgets(str, N, file);
        }
	fclose(file);
}
int get_year(const void* a, const void* b)
{
	return ((struct BOOK*)a)->year - ((struct BOOK*)b)->year;
}
void print_book(struct BOOK *line, int tmp)
{
	int i;
	for(i = 0; i < tmp; i++)
        {
        	printf("%d: %s,  %s,  %d\n", i+1, line[i].title, line[i].author, line[i].year);
        }
}
int compare_author (const void* a, const void* b)
{
	return strcmp(((struct BOOK*)a)->author, ((struct BOOK*)b)->author);
}
int counter()
{
	int tmp = 0;
	int i = 1;
	FILE *file;
	char str[N];
	file = fopen("book.txt", "r");
	while(fgets(str, N, file))
	{
		if((tmp + 1) / 4 != i)
		{
			tmp++;
		} else
			i++;
	}
	fclose(file);
	return tmp/3;
}
int main()
{
	struct BOOK *_book;
	int tmp;
	tmp = counter();
	_book = (struct BOOK *) malloc(tmp * sizeof(struct BOOK));
	get_book(_book, tmp);
	printf("\nCatalog books:\n");
	print_book(_book, tmp);
	qsort(_book, tmp, sizeof(struct BOOK), get_year);
	printf("\nMin year:\n");
	printf("%s,  %s,  %d\n", _book[0].title, _book[0].author, _book[0].year);
	printf("\nMax year:\n");
	printf("%s,  %s,  %d\n", _book[tmp-1].title, _book[tmp-1].author, _book[tmp-1].year);
	qsort(_book, tmp, sizeof(struct BOOK), compare_author);
    printf("\nSorted catalog:\n");
    print_book(_book, tmp);
	free(_book);
	return 0;
}
